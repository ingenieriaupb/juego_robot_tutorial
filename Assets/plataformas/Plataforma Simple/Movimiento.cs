﻿using UnityEngine;
using System.Collections;

public class Movimiento : MonoBehaviour {

    public GameObject plataforma;
    public Transform posicion_incial;
    public Transform posicion_final;
    private Transform posicion_siguiente;
    public float velocidad;


	// Use this for initialization
	void Start () {
        posicion_siguiente = posicion_final;
	}
	
	// Update is called once per frame
	void Update () {
        plataforma.transform.position = Vector2.MoveTowards(plataforma.transform.position, posicion_siguiente.position, Time.deltaTime * velocidad);

        if(plataforma.transform.position == posicion_siguiente.position)
        {
            posicion_siguiente = posicion_siguiente == posicion_final ? posicion_incial : posicion_final;
        }
	}
}
