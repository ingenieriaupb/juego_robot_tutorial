﻿using UnityEngine;
using System.Collections;

public class Hud : MonoBehaviour {

    public PlayerController heroe;
    public GameObject barra_vida;
    private Animator anim;
    public const string ESTADO_VIDAS = "Vidas";

	// Use this for initialization
	void Start () {
        anim = barra_vida.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        anim.SetInteger(ESTADO_VIDAS, heroe.Vidas);
	}
}
