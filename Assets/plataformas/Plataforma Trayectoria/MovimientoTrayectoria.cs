﻿using UnityEngine;
using System.Collections;

public class MovimientoTrayectoria : MonoBehaviour {

    public Transform centro;
    private float xo, yo,x,y, r, angulo, tiempo;

	// Use this for initialization
	void Start () {
        r = 5f;
        angulo = Mathf.PI/4;
        xo = centro.transform.localPosition.x;
        yo = centro.transform.localPosition.y;
        tiempo = 0f;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
	    if(tiempo >= 0.1f)
        {
            x = xo + r * Mathf.Cos(angulo);
            y = yo + r * Mathf.Sin(angulo);
            angulo = (angulo - Mathf.PI / 32) % (2*Mathf.PI);
            transform.position = new Vector2(x, y);
            tiempo = 0f;
        }
        else
        {
            tiempo += Time.deltaTime;
        }
	}
}
