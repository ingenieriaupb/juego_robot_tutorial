﻿using UnityEngine;
using System.Collections;

public class Puntaje : MonoBehaviour {

    public PlayerController heroe;
    public GameObject puntaje_0;
    public GameObject puntaje_1;
    public GameObject puntaje_2;
    private Animator[] anim = new Animator[3];
    private const string PUNTAJE = "Puntos";
    private string[] estados = { "Estado_00", "Estado_01" , "Estado_02" , "Estado_03" , "Estado_04" , "Estado_05" , "Estado_06" , "Estado_07" , "Estado_08" , "Estado_09" };
    private int contador = 0;
    private float delta = 0f;
    // Use this for initialization
    void Start () {
	    anim[0] = puntaje_0.GetComponent<Animator>();
	    anim[1] = puntaje_1.GetComponent<Animator>();
	    anim[2] = puntaje_2.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {

        /*if (delta > .1f)
        {
            ActualizarContador((int)Random.Range(0F, 1000F));
            //ActualizarContador(contador++);
            delta = 0f;
        }
        else
        {
            delta += Time.deltaTime;
        }*/
        ActualizarContador(heroe.Puntos);
    }

    public void ActualizarContador(int numero)
    {
        int unidades = numero % 10;
        int decenas = numero % 100 - unidades;
        int centenas = numero % 1000 - decenas;

        decenas = decenas / 10;
        centenas = centenas / 100;

        Debug.Log(centenas + " " + decenas + " " + unidades);

        if (numero > 99)
        {            
            anim[0].Play(estados[centenas]);
        }
        else
        {
            anim[0].Play(estados[0]);
        }

        if (numero > 9)
        {
            anim[1].Play(estados[decenas]);
        }
        else
        {
            anim[1].Play(estados[0]);
        }
        
        anim[2].Play(estados[unidades]);
    }
}
