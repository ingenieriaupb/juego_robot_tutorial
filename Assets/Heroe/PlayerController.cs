﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float altura_salto;
    public float velocidad_moviento;
    private Rigidbody2D rb;
    private Animator anim;
    private bool toco_piso;
    public LayerMask capa_piso;
    public float radio_validacion;
    public Transform validador_piso;
    private Vector2 pos_o;
    public const string MUERTE = "Muerte";
    public const string MONEDA = "Moneda";
    public const string PLATAFORMA_MOVIL = "PlataformaMovil";
    private int vidas = 3;
    private int puntos = 0;

    //Asigna o retorna las vidas del personaje
    public int Vidas
    {
        get
        {
            return vidas;
        }

        set
        {
            vidas = value;
        }
    }

    public int Puntos
    {
        get
        {
            return puntos;
        }

        set
        {
            puntos = value;
        }
    }


    // Use this for initialization
    void Start () {
        pos_o = this.transform.position;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
	}
    
    void OnCollisionEnter2D(Collision2D collider)
    {
        Debug.Log(collider.transform.tag);
        if (collider.transform.tag.Equals(PLATAFORMA_MOVIL))
        {
            transform.parent = collider.transform;
        }
        else
        {
            transform.parent = null;
        }

        if(collider.transform.tag.Equals(MUERTE))
        {
            if(--vidas > 0)
            {
                this.transform.position = pos_o;
            }
            else
            {
                Debug.Log("Se murio el heroe");
                this.enabled = false;
            }
        }
        
    }
    
    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log(collider.tag);
        if(collider.tag.Equals(MONEDA))
        {
            collider.gameObject.SetActive(false);
            puntos+=5;
        }
    } 
          
    void FixedUpdate()
    {
        //Debug.Log(tiempo);       
        toco_piso = Physics2D.OverlapCircle(validador_piso.position, radio_validacion, capa_piso);                          
    }


    // Update is called once per frame
    void Update() {

        if (toco_piso)
        {
            anim.SetInteger("Estado", 0);
        }       
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-velocidad_moviento, rb.velocity.y);
            rb.transform.localScale = new Vector2(-1, 1);
            anim.SetInteger("Estado",1);          
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocidad_moviento, rb.velocity.y);
            rb.transform.localScale = new Vector2(1, 1);
            anim.SetInteger("Estado", 1);
        }
        if(Input.GetKey(KeyCode.Space) && toco_piso)
        {
            rb.velocity = new Vector2(rb.velocity.x, altura_salto);
            anim.SetInteger("Estado", 2);
        }
    }
   
}
